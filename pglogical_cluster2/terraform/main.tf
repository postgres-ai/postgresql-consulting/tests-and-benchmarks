terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "${var.aws_deploy_region}"
}

locals {
  provider_tags = {
    Name = "${var.aws_deploy_ec2_provider_instance_tag_ansible}"
    Ansible_group = "${var.aws_deploy_ec2_provider_instance_tag_ansible}"
  }
}

locals {
  provider2_tags = {
    Name = "${var.aws_deploy_ec2_provider2_instance_tag_ansible}"
    Ansible_group = "${var.aws_deploy_ec2_provider2_instance_tag_ansible}"
  }
}

locals {
  subscriber_tags = {
    Name = "${var.aws_deploy_ec2_subscriber_instance_tag_ansible}"
    Ansible_group = "${var.aws_deploy_ec2_subscriber_instance_tag_ansible}"
  }
}

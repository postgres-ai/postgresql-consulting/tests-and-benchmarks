puts "SETTING CONFIGURATION"

dbset db pg
dbset bm tpc-h
diset connection pg_host localhost
diset connection pg_port 6666
diset tpch pg_scale_fact 1
diset tpch pg_num_tpch_threads 1
diset tpch pg_total_querysets 1
diset tpch pg_tpch_superuser postgres
diset tpch pg_tpch_superuserpass welcome1
diset tpch pg_tpch_defaultdbase pglogical
diset tpch pg_tpch_user postgres
diset tpch pg_tpch_pass welcome1
diset tpch pg_tpch_dbase pglogical_dss

print dict
buildschema
waittocomplete

puts "SCHEMA READY"

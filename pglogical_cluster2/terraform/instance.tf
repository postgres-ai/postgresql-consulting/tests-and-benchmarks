resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "provision_key" {
  key_name   = "${var.aws_deploy_ec2_key_name}"
  public_key = tls_private_key.ssh_key.public_key_openssh
}

resource "null_resource" ssh_key {
  provisioner "local-exec" { # save private key locally
    command = "echo '${tls_private_key.ssh_key.private_key_pem}' > ../${var.aws_deploy_ec2_key_name}.pem"
  }
  provisioner "local-exec" {
    command = "chmod 600 ../'${var.aws_deploy_ec2_key_name}'.pem"
  }
}

resource "aws_spot_instance_request" "postgres_ec2_provider" {
  ami               = "${data.aws_ami.ami.id}"
  availability_zone = "${var.aws_deploy_availability_zone}"
  instance_type     = "${var.aws_deploy_ec2_instance_type}"
  security_groups   = ["${aws_security_group.security_group.name}"]
  key_name          = aws_key_pair.provision_key.key_name
  spot_price        = "${var.aws_spot_price}"
  wait_for_fulfillment = true

  root_block_device {
    volume_size = "${var.aws_deploy_root_ebs_size}"
  }
}

resource "aws_spot_instance_request" "postgres_ec2_provider2" {
  ami               = "${data.aws_ami.ami.id}"
  availability_zone = "${var.aws_deploy_availability_zone}"
  instance_type     = "${var.aws_deploy_ec2_instance_type}"
  security_groups   = ["${aws_security_group.security_group.name}"]
  key_name          = aws_key_pair.provision_key.key_name
  spot_price        = "${var.aws_spot_price}"
  wait_for_fulfillment = true

  root_block_device {
    volume_size = "${var.aws_deploy_root_ebs_size}"
  }
}

resource "aws_spot_instance_request" "postgres_ec2_subscriber" {
  ami               = "${data.aws_ami.ami.id}"
  availability_zone = "${var.aws_deploy_availability_zone}"
  instance_type     = "${var.aws_deploy_ec2_instance_type}"
  security_groups   = ["${aws_security_group.security_group.name}"]
  key_name          = aws_key_pair.provision_key.key_name
  spot_price        = "${var.aws_spot_price}"
  wait_for_fulfillment = true

  root_block_device {
    volume_size = "${var.aws_deploy_root_ebs_size}"
  }
}

resource "aws_ec2_tag" "postgres_ec2_provider_tags" {
  resource_id = aws_spot_instance_request.postgres_ec2_provider.spot_instance_id
  for_each = local.provider_tags
  key      = each.key
  value    = each.value
}

resource "aws_ec2_tag" "postgres_ec2_provider2_tags" {
  resource_id = aws_spot_instance_request.postgres_ec2_provider2.spot_instance_id
  for_each = local.provider2_tags
  key      = each.key
  value    = each.value
}

resource "aws_ec2_tag" "postgres_ec2_subscriber_tags" {
  resource_id = aws_spot_instance_request.postgres_ec2_subscriber.spot_instance_id
  for_each = local.subscriber_tags
  key      = each.key
  value    = each.value
}

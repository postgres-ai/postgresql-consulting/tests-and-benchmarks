#!/bin/bash

# This script triggers a pipeline with a "dedicated" test and monitors its execution status.
# Upon the pipeline's completion, it checks the status of each job. If any of the jobs fail, the entire test run is considered a failure.

GITLAB_PERSONAL_TOKEN="${GITLAB_PERSONAL_TOKEN:-''}"
GITLAB_PRIVATE_TOKEN="${GITLAB_PRIVATE_TOKEN:-''}"
GITLAB_PIPELINE_API_URL="https://gitlab.com/api/v4/projects/25094117"
GITLAB_PIPELINE_BASE_URL="https://gitlab.com/postgres-ai/postgresql-consulting/tests-and-benchmarks"

BRANCH="${BRANCH:-master}"
TEST_MODE="${TEST_MODE:-dedicated}"
TEST_RUNS=$(cat test/test_runs_${TEST_MODE}.json)
TEST_RUNS_ESCAPED=$(echo "$TEST_RUNS" | jq -aRs .)

# run test pipeline
PIPELINE_ID=$(curl --request POST \
  --header "Content-Type: application/json" \
  --data "{
    \"token\": \"$GITLAB_PERSONAL_TOKEN\",
    \"ref\": \"$BRANCH\",
    \"variables\": {
      \"MODE\": \"$TEST_MODE\",
      \"SERVER_TYPE\": \"CCX13\",
      \"VOLUME_SIZE\": \"50\",
      \"POSTGRES_VERSION\": \"17\",
      \"PGBENCH_INIT_COMMAND\": \"pgbench -i -q -s 10\",
      \"CHATS_REQUESTED_MESSAGE_ID\": \"123\",
      \"TEST_RUNS\": $TEST_RUNS_ESCAPED
    }
  }" \
  "${GITLAB_PIPELINE_API_URL}/trigger/pipeline" | jq -r .id)

# Checking that the pipeline ID has been received
if [ -z "$PIPELINE_ID" ] || [ "$PIPELINE_ID" == "null" ]; then
  echo "ERROR: The pipeline ID was not received."
  exit 1
else
  echo "$(date +'%Y-%m-%d %H:%M:%S') INFO: Pipeline ID ${PIPELINE_ID} started"
fi

# Initial delay before checking the pipeline status
sleep 10

# Check pipeline status
while true; do
  PIPELINE_STATUS=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "${GITLAB_PIPELINE_API_URL}/pipelines/${PIPELINE_ID}" | jq -r .status)

  echo "Pipeline Status: $PIPELINE_STATUS"

  if [[ "$PIPELINE_STATUS" == "canceled" ]]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') INFO: Pipeline ${PIPELINE_ID} was canceled."
    exit 1
  elif [[ "$PIPELINE_STATUS" == "failed" ]]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') ERROR: Pipeline ${PIPELINE_ID} has failed. Details: ${GITLAB_PIPELINE_BASE_URL}/-/pipelines/${PIPELINE_ID}"
    exit 1
  elif [[ "$PIPELINE_STATUS" == "success" ]]; then
    # Get a list of jobs and their statuses
    JOBS_JSON=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "${GITLAB_PIPELINE_API_URL}/pipelines/$PIPELINE_ID/jobs")

    # Check the status of each job
    FAILED_JOB_COUNT=$(echo ${JOBS_JSON} | jq '[.[] | select(.status != "success")] | length')

    # If the number of unsuccessful jobs > 0, we consider the pipeline completed with an error
    if [[ "${FAILED_JOB_COUNT}" -gt 0 ]]; then
      echo "$(date +'%Y-%m-%d %H:%M:%S') ERROR: Pipeline ${PIPELINE_ID} completed with an error, unsuccessful jobs were found: ${FAILED_JOB_COUNT}"

      # Output job statuses for debugging/information
      echo "Pipeline job statuses:"
      echo "${JOBS_JSON}" | jq -r '.[] | "\(.name): \(.status)"'

      echo "Details: ${GITLAB_PIPELINE_BASE_URL}/-/pipelines/${PIPELINE_ID}"
      exit 1
    else
      echo "$(date +'%Y-%m-%d %H:%M:%S') INFO: Pipeline ${PIPELINE_ID} completed successfully. Details: ${GITLAB_PIPELINE_BASE_URL}/-/pipelines/${PIPELINE_ID}"
      exit 0
    fi

  else
    echo "$(date +'%Y-%m-%d %H:%M:%S') INFO: Pipeline ${PIPELINE_ID} is still in progress..."
  fi

  sleep 30 # Check every 30 seconds
done

select pglogical.create_node(
  node_name := 'subscriber',
  dsn := 'host=pg-subscriber port={{ pg_port }} dbname={{ pg_db_name }} user=postgres password={{ pg_password }}'
);

select pglogical.create_subscription(
subscription_name := 'subscription1',
provider_dsn := 'host=pg-provider port={{ pg_port }} dbname={{ pg_db_name }} user=postgres password={{ pg_password }}'
);

create table second_provider_test (
    id             serial,
    username       text not null,
    password       text,
    created_on     timestamptz not null,
    last_logged_on timestamptz not null,
    CONSTRAINT prt_test_pk PRIMARY KEY (id, created_on)
);


SELECT pglogical.create_subscription(
    subscription_name := 'subscription2',
    replication_sets := array['second_provider_repl_set'],
    synchronize_structure := false,
    provider_dsn := 'host=pg-provider2 port=6666 dbname=pglogical user=postgres password=welcome1'
);



SELECT pglogical.create_node(
    node_name := 'provider',
    dsn := 'host=pg-provider port={{ pg_port }} dbname={{ pg_db_name }} user=postgres password={{ pg_password }}'
);

alter table public.history REPLICA IDENTITY FULL;

SELECT pglogical.replication_set_add_table('default_insert_only', 'public.history');

SELECT pglogical.replication_set_add_table('default', 'customer');
SELECT pglogical.replication_set_add_table('default', 'district');
SELECT pglogical.replication_set_add_table('default', 'item');
SELECT pglogical.replication_set_add_table('default', 'new_order');
SELECT pglogical.replication_set_add_table('default', 'order_line');
SELECT pglogical.replication_set_add_table('default', 'orders');
SELECT pglogical.replication_set_add_table('default', 'stock');
SELECT pglogical.replication_set_add_table('default', 'warehouse');
